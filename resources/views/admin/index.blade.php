@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="container-fluid">
            <div class="row">
                <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                    <div class="sidebar-sticky">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link active" href="#">
                                    <span data-feather="home"></span>
                                    MainPanel
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <span data-feather="file"></span>
                                    Orders
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <span data-feather="users"></span>
                                    Users
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h1 class="h2">MainPanel</h1>
                    </div>
                    <div class="row">
                    <div>
                        <img src="">
                        <div>
                            <div>
                                <input type="file">
                            </div>
                        </div>
                    </div>
                    <div>
                        <div>
                            <label>Text</label>
                        </div>
                        <textarea></textarea>
                    </div>
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Save</button>
                    </div>
                    <h2>Users</h2>
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Password</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>nikita</td>
                                <td>nik@mail.ru</td>
                                <td>adminroot</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </main>
            </div>
        </div>
@endsection
