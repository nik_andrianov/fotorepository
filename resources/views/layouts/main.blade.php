<main role="main" class="container">
    <div class="">
        <h1>Загрузка файла</h1>
        <div class="lead">
            <div class="form-horizontal" role="form" method="POST" action="">
                {{ csrf_field() }}
                <div class="form-group">
                    <img src="">
                    <input type="file" name="page">
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label"><b>Краткое описсание:</b></label>
                    <textarea type="text" class="form-control font-weight-normal" name="body"></textarea>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label"><b>E-mail:</b></label>
                    <input type="text" class="form-control font-weight-normal" name="email">
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-sign-in">Загрузить</i>
                        </button>
                    </div>
            </div>
        </div>
    </div>
</main>