<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'IndexController@index');
Route::get('/admin', 'AdminController@index');
Route::get('/admin/show', 'AdminController@show');
Route::get('/admin/show/{post}', 'AdminController@show');
Route::auth();

Route::match(['get'], 'register', function (){
    return redirect('/');
});
//Route::get('/admin/', function () {
////    $tasks = DB::table('tasks')->get();
////    $tasks = App\Task::all();
//    $tasks = App\Task::incompleted();
//    return view('admin.index', compact('tasks'));
//});
//Route::get('/admin/{task}', function ($id) {
////    $task = DB::table('tasks')->find($id);
//    $task = App\Task::find($id);
//    return view('admin.show', compact('task'));
//});

