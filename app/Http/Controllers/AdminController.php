<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
//        $tasks = App\Task::all();
//        return view('admin.index', compact('tasks'));
        return view('admin.index');
    }
//    public function show($id){
//        $task = App\Task::find($id);
//        return view('admin.show', compact('task'));
//    }
}
