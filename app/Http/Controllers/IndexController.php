<?php

namespace App\Http\Controllers;
use App\Task;

use Illuminate\Http\Request;

use App\Http\Requests;

class IndexController extends Controller
{
    public function index(){
        return view('main.main');
    }
}
