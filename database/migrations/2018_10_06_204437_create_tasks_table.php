<?php
use ORM;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alias');
            $table->text('body');
            $table->text('email');
            $table->text('page');
            $table->timestamps();
        });
//        ORM::configure(array('connection_string' => 'mysql:host=127.0.0.1;dbname=fotorepo', 'username=admin', 'password=root'));
//        $faker = Faker\Factory::create('ru_RU');
//        for ($i=0;$i<10;$i++){
//            $employeer = ORM::for_table('employeer')->create();
//            $employeer->body = $faker->realText(100);
//            $employeer->email = $faker->email;
//            $employeer->photo = $faker->imageUrl(200, 200, 'people');
//            $employeer->save();
//
//        }
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tasks');
    }
}
